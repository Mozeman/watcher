using System;
using System.Diagnostics.CodeAnalysis;
using Smod2.API;

namespace Watcher
{
	// This class MUST match WatcherMessageJson from PHP and ALL fields must be set. Failure to do so will result with input-validation errors.
	[SuppressMessage("ReSharper", "InconsistentNaming")] // Same names as PHP
	[SuppressMessage("ReSharper", "NotAccessedField.Global")] // Being accessed for JSON serialization lol
	public struct WatcherMessageJson
	{
		public string auth;

		public string player_ip;
		public string server_address;
		public string server_name;
		public int server_players_current;
		public int server_players_max;

		public string version;

		public long timestamp;

		public WatcherMessageJson(Watcher plugin, Player player, Server server)
		{
			auth = player.GetAuthToken();

			player_ip = player.IpAddress.Replace("::ffff:", string.Empty); // Skip "::ffff:"
			server_address = server.IpAddress + ":" + server.Port;
			server_name = server.Name;
			server_players_current = server.NumPlayers - 1;
			server_players_max = ((CustomNetworkManager) CustomNetworkManager.singleton).ReservedMaxPlayers;

			version = plugin.Details.version;

			timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
		}
	}
}