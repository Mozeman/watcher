using System.Collections.Generic;
using Smod2.EventHandlers;
using Smod2.Events;

namespace Watcher
{
	internal class WaitingForPlayersHandler : IEventHandlerWaitingForPlayers
	{
		private readonly Watcher plugin;

		public WaitingForPlayersHandler(Watcher plugin) => this.plugin = plugin;

		public void OnWaitingForPlayers(WaitingForPlayersEvent ev)
		{
			plugin.Info("Setting rate limiters for watchlists...");
			Dictionary<string, RateLimiter> newRateLimiters = new Dictionary<string, RateLimiter>();
			foreach (string watchlist in plugin.watchlists)
			{
				plugin.Debug("Setting rate limiter for watchlist " + watchlist);
				newRateLimiters.Add(
					watchlist,
					plugin.rateLimiters.TryGetValue(watchlist, out RateLimiter rateLimiter)
						? rateLimiter
						: new RateLimiter(plugin, watchlist)
				);
			}

			plugin.rateLimiters = newRateLimiters;
			plugin.Info("Finished setting rate limiters for " + plugin.watchlists.Length + " watchlists.");
		}
	}
}