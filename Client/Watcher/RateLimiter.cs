using System;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.Networking;

namespace Watcher
{
	public class RateLimiter
	{
		private readonly Watcher plugin;
		private readonly AutoKick autoKick = new AutoKick();
		private readonly string watchlist;
		private readonly Queue<UnityWebRequest> queued;

		private CoroutineHandle? worker;

		public bool Initialized { get; private set; }
		public int Max { get; private set; }
		public long Reset { get; private set; }
		public int Remaining { get; private set; }

		public long TimeToReset => Reset - DateTimeOffset.UtcNow.ToUnixTimeSeconds();

		public RateLimiter(Watcher plugin, string watchlist)
		{
			this.plugin = plugin;
			this.watchlist = watchlist;

			queued = new Queue<UnityWebRequest>();
		}

		public void Enqueue(UnityWebRequest request)
		{
			queued.Enqueue(request);

			if (worker == null)
			{
				worker = Timing.RunCoroutine(_Worker());
			}
		}

		public IEnumerator<float> _Worker()
		{
			while (queued.Count > 0)
			{
				while (queued.Count > 0 && (!Initialized || Remaining > 0))
				{
					yield return Timing.WaitUntilDone(_Send(queued.Dequeue()));
				}

				yield return Timing.WaitForSeconds(TimeToReset);
				Remaining = Max;
			}

			worker = null;
		}

		private IEnumerator<float> _Send(UnityWebRequest request)
		{
			yield return Timing.WaitUntilDone(request.SendWebRequest());

			if (request.isHttpError || request.isNetworkError)
			{
				plugin.Error($"Failed to contact watchlist (HTTP error): {watchlist}\n[{request.responseCode}] {request.downloadHandler.text}");
			}
			else if (request.isNetworkError)
			{
				plugin.Error($"Failed to contact watchlist (network error): {watchlist}\n{request.error}");
			}
			else if (request.responseCode == 206 && plugin.autoDisconnect)
			{
				autoKick.KickHandler(plugin, request.downloadHandler.text);
			}
			else if (request.responseCode != 204)
			{
				plugin.Info($"Message from watchlist: {watchlist}\n{request.downloadHandler.text}");
			}

			string headerMax = request.GetResponseHeader("X-RateLimit-Max");
			string headerReset = request.GetResponseHeader("X-RateLimit-Reset");
			string headerRemaining = request.GetResponseHeader("X-RateLimit-Remaining");
			if (
				headerMax == null || headerReset == null || headerRemaining == null ||
				!int.TryParse(headerMax, out int max) ||
				!long.TryParse(headerReset, out long reset) ||
				!int.TryParse(headerRemaining, out int remaining)
			)
			{
				plugin.Error($"Failed to update rate limiter of watchlist: {watchlist}\nA server-side fatal error likely occured.");
				yield break;
			}

			Max = max;
			Reset = reset;
			Remaining = remaining;

			Initialized = true;
		}
	}
}