﻿using Smod2.API;
using UnityEngine;

namespace Watcher
{
	class AutoKick
	{
		public void KickHandler(Watcher plugin, string Response)
		{
			string[] Data = Response.Split('~');

			foreach (Player pl in plugin.Server.GetPlayers())
			{
				if (pl.SteamId == Data[0])
				{
					if (!((GameObject)pl.GetGameObject()).GetComponent<ServerRoles>().BypassStaff)
					{
						pl.Disconnect("You have been disconnected from the server!\nReason: " + Data[1]);
						plugin.Info(pl.Name + " was disconnected from the server by Watcher. Reason: " + Data[1]);
						if (plugin.autoBan) plugin.Server.BanIpAddress(pl.Name, pl.IpAddress, 30000000, Data[1], "Watcher");
					}
				}
			}
		}
	}
}
