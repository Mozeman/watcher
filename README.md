# Watcher

Plugin that will detect if a known bad player joins the server, and notify global mods

| Config Option| Default value  | Description  |
|:-------------:|:---------------:|:---------------:|
| w_disable | false | Disables the watcher plugin |
| w_gmod_slot | true | Automatically grants global moderators reserved slots upon joining the server |
| w_watchlists |  | Specifies secondary URLs to send the data to. |
| w_auto_disconnect | true | Disables the auto disconnect feature of watcher |
| w_auto_ban | true | Automatically ip bans any users disconnected by auto disconnect |

## Secondary watchlist
The secondary watchlists config option allows a server host to double up the watcher plugin as both a global and local watcher, meaning that they could write their own script to alert them when a wanted user joins the server, allowing them to deal with it (Useful for if you want to watch out for known trolls to keep returning, or so you can deal with any previous issues they have caused).  
To know what data will be sent, see `Client/Watcher/WatcherMessageJson.cs`