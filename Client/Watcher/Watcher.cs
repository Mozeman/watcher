using System.Collections.Generic;
using System.IO;
using System;
using Smod2;
using Smod2.Attributes;
using Smod2.Config;
using Smod2.Events;

namespace Watcher
{

	[PluginDetails(
		author = "Phoenix",
		name = "Watcher",
		description = "Watches your server for those bad bois",
		id = "Phoenix.Watcher",
		configPrefix = "w",
		version = "1.2",
		SmodMajor = 3,
		SmodMinor = 4,
		SmodRevision = 1
	)]
	public class Watcher : Plugin
	{
		public const string kPrimaryServer = "https://corruptionbot.xyz/watcher/watcher.php";

		[ConfigOption]
		public readonly bool disable = false;
		[ConfigOption]
		public readonly bool gmodSlot = true;
		//Automatically disconnects and bans users if/when requested by Watcher
		[ConfigOption]
		public readonly bool autoDisconnect = true;
		//Automatically bans that IP from the server, preventing them from joining again
		[ConfigOption]
		public readonly bool autoBan = true;

		public string[] watchlists = new string[0];

		public static string appDataFolder = FileManager.GetAppFolder();
		public static readonly string watchlistsFilePath = appDataFolder + Path.DirectorySeparatorChar + "Watcher" + Path.DirectorySeparatorChar + "WatchLists.txt";

		internal Dictionary<string, RateLimiter> rateLimiters;
		internal readonly RateLimiter primaryRateLimiter;

		public Watcher()
		{
			rateLimiters = new Dictionary<string, RateLimiter>();
			primaryRateLimiter = new RateLimiter(this, "primary");
		}

		public override void OnDisable() { } // SMod auto-logs disabling

		public override void OnEnable()
		{
			if (disable) PluginManager.DisablePlugin(this);
			// SMod auto-logs enabling

			CreateWatchlistsFile();
			watchlists = LoadWatchlists();
		}

		public override void Register()
		{
			// Registered as low so auto-disconnect (e.g. vpnshield) can run before this, preventing unnecessary pings.
			AddEventHandlers(new JoinEventHandler(this), Priority.Low);
			AddEventHandlers(new WaitingForPlayersHandler(this));
		}

		public void CreateWatchlistsFile()
		{
			this.Debug("Checking for Watcher main folder...");
			if (!Directory.Exists(appDataFolder + Path.DirectorySeparatorChar + "Watcher"))
			{
				// Create Directory if not existing
				this.Debug($"Creating folder Watcher...");
				Directory.CreateDirectory(appDataFolder + Path.DirectorySeparatorChar + "Watcher");
				this.Debug($"Created folder Watcher. {appDataFolder + Path.DirectorySeparatorChar + "Watcher"}");
			}
			if (!File.Exists(watchlistsFilePath))
			{
				// Create File if not existing and then add comment about File's Purpose
				this.Debug("Creating file Watchlists.txt in Watchers Folder...");
				File.WriteAllText(watchlistsFilePath, "#Add Watchlist URLs to send Watcher data https://gitlab.com/Phoenix--Project/watcher \n#Empty lines and comments with '#' will be ignored\n");
				this.Debug($"Created file WatchLists.txt in {appDataFolder + Path.DirectorySeparatorChar + "Watcher"}");
			}
		}

		public string[] LoadWatchlists()
		{
			this.Info("Loading Watchlists from WatchLists.txt ...");
			List<string> watchlist = new List<string>();
			string[] lines = File.ReadAllLines(watchlistsFilePath);
			foreach (string l in lines)
			{
				string line = l.Replace("\n", "");
				this.Debug("Parsing watchlist from line :\n" + line);

				if (line.StartsWith("#")) continue;

				if (Uri.IsWellFormedUriString(line, UriKind.Absolute))
				{
					watchlist.Add(line);
					this.Debug("Loaded a watchlist into watcher.watchlists" + line);
				}
			}
			this.Info($"Finished loading {watchlist.Count} watchlists from file.");
			return watchlist.ToArray();
		}
	}
}
